import * as dotenv from 'dotenv' 
dotenv.config()
import express from 'express'
const app = express()
import bodyParser from 'body-parser';
import router from './routes/index.js'
import cors from 'cors';
import {dbConnection} from './dbConnection.js'
const port = process.env.PORT || 3199

app.use(bodyParser.json());
app.use(cors());
app.use('/api',router)
app.listen(port,()=>{
	console.log(`Server is running......${port}`)
})

dbConnection ()

