import User from "../models/User.model.js"
import bcrypt from "bcrypt";
import { response } from "express";
import jwt from 'jsonwebtoken'
export const registerUser = async(req,res)=>{
    try{
        console.log(req.body)
        const {username,email,password} = req.body
        const user = await User.findOne({ where: { email: email} });
        if(user){
            return res.json("Email already used")
        }else{
            const hashedPassword = await bcrypt.hash(password, 10)
            console.log(hashedPassword)
            const userdata = {
                username:username,
                email:email,
                password:hashedPassword
            }
            const newUser = User.build(userdata) 
            await newUser.save();
            return res.json("user created successfully")
        }
    }catch(err){
        console.log("Error in registerUser ",err)
    }

}
export const logInUser = async(req,res)=>{
    try{
        const {email,password} = req.body
        const user = await User.findOne({ where: { email: email} });
        if(!user){
            return res.json("User not exist")
        }else{
            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) return res.status(400).json("Invalid credentials.");
            const token = jwt.sign({id:user.id},process.env.JWT_SECRET)
            res.json({"token":token})
        }
    }catch(err){
        console.log("Error in logInUser",err)
    }

}
export const updateUser  = async(req,res)=>{
    try{
        const updateObj = req.body
        User.update(updateObj,{where: {id:req.user}})
        return res.json("User updated successfully")
    }catch(err){
        console.log("Error in updateUser",err)
    }

}
export const deleteUser = async(req,res)=>{
    try{
        User.destroy({
            where: {id:req.user}
          })
        return res.json("User deleted successfully")
    }catch(err){
        console.log("Error in deleteUser",err)
    }

}
export const fetchUserById = async(req,res)=>{
    try{
        console.log(req.user)
        const user = await User.findOne({ where: { id: req.user} });
        res.json({
            "user":user,
            "token":req.header("x-auth-token")
        });
    }catch(err){
        console.log("Error in fetch user",err)
    }
}

