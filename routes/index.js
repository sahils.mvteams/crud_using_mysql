import Router from 'express'
import {auth} from '../middlewares/auth.middleware.js'
const router = Router()
import {registerUser,logInUser,updateUser,deleteUser,fetchUserById} from '../controllers/User.Controller.js'
router.post('/register',registerUser)
router.get('/login',logInUser)
router.put('/updateUserById',auth,updateUser)
router.delete('/deleteUser',auth,deleteUser) 
router.get('/fetchUser',auth,fetchUserById)

export default router