import dotenv from 'dotenv'
dotenv.config()
import { Sequelize } from 'sequelize';
// console.log(process.env.DB_PASSWORD)
export const sequelize = new Sequelize(process.env.DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'mysql' 
});

export async function dbConnection (){
    try{
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    }catch(err){
        console.error('Unable to connect to the database:', error);
    }
}

