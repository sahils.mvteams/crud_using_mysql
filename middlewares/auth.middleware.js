import  jwt from 'jsonwebtoken';
export const auth = (req, res, next) => {
try{
    const token = req.headers.authorization.split(' ')[1];
    if(!token){
        return res.status(401).json("No authentication token, access denied");
    }
    const verified = jwt.verify(token, process.env.JWT_SECRET);
    if(!verified){
        return res.status(401).json("Token verification failed, authorization denied");
    }
    console.log(verified)
    req.user = verified.id;
    next();
} catch (err) {
res.status(500).json({ error: err.message });
}
}
