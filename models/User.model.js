import { Sequelize, DataTypes, Model } from 'sequelize';
import {sequelize} from '../dbConnection.js';


class User extends Model {}

User.init({
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  username: {
    type: DataTypes.STRING,
  },
  email: {
    type: DataTypes.STRING
    
    // allowNull defaults to true
  },
  password:{
    type: DataTypes.STRING
  }
}, {
  // Other model options go here
  sequelize, // We need to pass the connection instance
  modelName: 'User' // We need to choose the model name
});

// the defined model is the class itself
await User.sync();

export default User